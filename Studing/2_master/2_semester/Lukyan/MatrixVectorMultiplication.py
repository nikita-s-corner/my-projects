from mpi4py import MPI
from numpy import empty, zeros, array, int32, float64, dot

comm = MPI.COMM_WORLD
numprocs = comm.Get_size()
rank = comm.Get_rank()

if rank == 0 :
    f1 = open('in.dat', 'r')
    N = array(int32(f1.readline()))
    M = array(int32(f1.readline()))
    f1.close()
else:
    N = array(0, dtype=int32)
  
comm.Bcast([N, 1, MPI.INT], root=0)

if rank == 0 :
    ave, res = divmod(M, numprocs-1)
    rcounts = empty(numprocs, dtype=int32)
    displs = empty(numprocs, dtype=int32)
    rcounts[0] = 0; displs[0] = 0
    for k in range(1, numprocs) : 
        if k < 1 + res :
            rcounts[k] = ave + 1
        else :
            rcounts[k] = ave
        displs[k] = displs[k-1] + rcounts[k-1]
else :
    rcounts = None
    displs = None
    
M_part = array(0, dtype=int32)
    
comm.Scatter([rcounts, 1, MPI.INT], [M_part, 1, MPI.INT], root=0)    
 	
if rank == 0 :
    f2 = open('AData.dat', 'r')
    for k in range(1, numprocs) :
        A_part = empty((rcounts[k], N), dtype=float64)
        for j in range(rcounts[k]) :
            for i in range(N) :
                A_part[j,i] = float(f2.readline())
        comm.Send([A_part, rcounts[k]*N, MPI.DOUBLE], dest=k, tag=0)
    f2.close()
    A_part = empty((M_part, N), dtype=float64)
else :
    A_part = empty((M_part, N), dtype=float64)
    comm.Recv([A_part, M_part*N, MPI.DOUBLE], source=0, tag=0)

x = empty(N, dtype=float64)	
if rank == 0:
    f3 = open('xData.dat', 'r')
    for i in range(N) :
        x[i] = float(f3.readline())
    f3.close()

comm.Bcast([x, N, MPI.DOUBLE], root=0)

b_part = empty(M_part, dtype=float64)

b_part = dot(A_part, x)

# print('For rank={} : b_part={}'.format(rank,b_part))

# if rank == 0 :
#     status = MPI.Status()
#     b = zeros(M, dtype=float64)
#     for k in range(1, numprocs) :
#         comm.Probe(source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG, status=status)
#         comm.Recv([b[displs[status.Get_source()]:], rcounts[status.Get_source()], MPI.DOUBLE],
#                   source=status.Get_source(), tag=0, status=None)
# else :
#     comm.Send([b_part, rcounts[rank], MPI.DOUBLE], dest=0, tag=0)

if rank == 0 :
    b = empty(M, dtype=float64)
else :
    b = None

comm.Gatherv([b_part, M_part, MPI.DOUBLE], 
             [b, rcounts, displs, MPI.DOUBLE], root=0)

if rank == 0:
    print(b)


# # Сохраняем результат вычислений в файл
# f4 = open('Results.dat', 'w')
# for j in range(M) :
#     f4.write(str(b[j])+'\n')
# f4.close()

# print(b)