import numpy as np
from mpi4py import MPI
from numpy import int32

comm = MPI.COMM_WORLD
numprocs = comm.Get_size()
rank = comm.Get_rank()

#read size of the matix

if rank == 0 :
    f1 = open('size.txt', 'r')
    N = np.array(int32(f1.readline()))
    f1.close()
else:
    N = np.array(0, dtype=int32)
    
#computing rcount and dispals

if rank == 0 :
    ave, res = divmod(N, numprocs)
    rcounts = np.empty(numprocs, dtype=int32)
    displs = np.empty(numprocs, dtype=int32)
    for k in range(numprocs) : 
        if k < res :
            rcounts[k] = ave + 1
        else :
            rcounts[k] = ave
        if k!=0: 
            displs[k] = displs[k-1] + rcounts[k-1]
        else:
            displs[0]=0
else :
    rcounts = None
    displs = None

N_part=np.array(0)    
comm.Scatter([rcounts, 1, MPI.INT], [N_part, 1, MPI.INT], root=0)

#reading and scattering a, b, c, d

if rank ==0 :
    f2=open("a.txt","r")
    f3=open("b.txt","r")
    f4=open("c.txt","r")
    f5=open("d.txt","r")
    f6=open("x.txt","r") 
    a=np.zeros(N)
    b=np.zeros(N)
    c=np.zeros(N)
    d=np.zeros(N)
    x_true=np.zeros(N)
    for i in range(N):
        a[i]=float(f2.readline())
        b[i]=float(f3.readline())
        c[i]=float(f4.readline())
        d[i]=float(f5.readline())
        x_true[i]=float(f6.readline())
    f2.close()
    f3.close()
    f4.close()
    f5.close()
    f6.close()
else :
    a=None
    b=None
    c=None
    d=None
a_part=np.zeros(N_part)
b_part=np.zeros(N_part)
c_part=np.zeros(N_part)
d_part=np.zeros(N_part)

comm.Scatterv([a, rcounts, displs, MPI.DOUBLE],[a_part, N_part, MPI.DOUBLE],root=0)
comm.Scatterv([b, rcounts, displs, MPI.DOUBLE],[b_part, N_part, MPI.DOUBLE],root=0)
comm.Scatterv([c, rcounts, displs, MPI.DOUBLE],[c_part, N_part, MPI.DOUBLE],root=0)
comm.Scatterv([d, rcounts, displs, MPI.DOUBLE],[d_part, N_part, MPI.DOUBLE],root=0)


#part 1 (pseudo diagonalization)


coef=b_part[1]/a_part[0]
b_part[1]=c_part[1]-b_part[0]*coef
c_part[1]=-c_part[0]*coef
d_part[1]-=d_part[0]*coef
for i in range(2,N_part):
    coef1=c_part[i]/a_part[i-2]
    coef2=b_part[i]/a_part[i-1]
    c_part[i]=-c_part[i-2]*coef1-c_part[i-1]*coef2
    b_part[i]=-b_part[i-2]*coef1-b_part[i-1]*coef2
    d_part[i]=d_part[i]-d_part[i-2]*coef1-d_part[i-1]*coef2
    
    
#part 2 solving small system

if rank==0:
    a_small=np.zeros(2*numprocs)
    b_small=np.zeros(2*numprocs)
    c_small=np.zeros(2*numprocs)
    d_small=np.zeros(2*numprocs)
    a_small[0]=a_part[-2]
    a_small[1]=a_part[-1]
    d_small[0]=d_part[-2]
    d_small[1]=d_part[-1]
    for i in range(1,numprocs):
        message=np.zeros(8)
        comm.Recv([message, 8, MPI.DOUBLE], source=i)
        a_small[i*2]=message[0]
        a_small[i*2+1]=message[1]
        b_small[i*2]=message[2]
        b_small[i*2+1]=message[3]
        c_small[i*2]=message[4]
        c_small[i*2+1]=message[5]
        d_small[i*2]=message[6]
        d_small[i*2+1]=message[7]
    
    
    x=np.empty(N)
    x_small=np.zeros(2*numprocs)
    for i in range(2):
        x_small[i]=d_small[i]/a_small[i]
        
    x[displs[1]-2]=x_small[0]
    x[displs[1]-1]=x_small[1]
    
    for i in range(1,numprocs):
        x_small[i*2]=(d_small[i*2]-c_small[i*2]*x_small[i*2-2]-b_small[i*2]*x_small[i*2-1])/a_small[i*2]
        x_small[i*2+1]=(d_small[i*2+1]-c_small[i*2+1]*x_small[i*2-2]-b_small[i*2+1]*x_small[i*2-1])/a_small[i*2+1]  
        x[displs[i]+rcounts[i]-2]=x_small[i*2]
        x[displs[i]+rcounts[i]-1]=x_small[i*2+1]

    for i in range(numprocs-1):
        comm.Send([x_small[2*i:2*i+2:], 2, MPI.DOUBLE], dest=i+1)

    for i in range(rcounts[0]-2):
        x[i]=d_part[i]/a_part[i]
        
    for i in range(1,numprocs):
        x_proc=np.empty(rcounts[i]-2)
        comm.Recv([x_proc, rcounts[i]-2, MPI.DOUBLE], source=i)
        for j in range(rcounts[i]-2):
            x[j+displs[i]]=x_proc[j]
            
            

    print(np.abs(x-x_true).sum()) #answer
  
else:
    message=np.array([a_part[-2],a_part[-1],b_part[-2],b_part[-1],c_part[-2],c_part[-1],d_part[-2],d_part[-1]])    
    comm.Send([message, 8, MPI.DOUBLE], dest=0)
    
    x_part=np.empty(2)
    comm.Recv([x_part, 2, MPI.DOUBLE], source=0)
    
    x_proc=np.empty(N_part-2)
    for i in range(N_part-2):
        x_proc[i]=(d_part[i]-c_part[i]*x_part[0]-b_part[i]*x_part[1])/a_part[i]
        
    comm.Send([x_proc, N_part-2, MPI.DOUBLE], dest=0)
    
       



        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        










