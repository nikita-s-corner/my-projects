from mpi4py import MPI
import numpy as np
from numpy import int32

comm = MPI.COMM_WORLD
numprocs = comm.Get_size()
rank = comm.Get_rank()


#random matrix generation

N=9
col=int(np.sqrt(N))
row=col

A_part=np.array([[np.random.random() for i in range(col)] for j in range(row)])

if rank==0:
    b=np.array([np.random.random() for i in range(N)])
else:
    b=np.zeros(N)

comm_row=comm.Split(rank // col,rank)
comm_col=comm.Split(rank % row,rank)
rank_row=comm_row.Get_rank()
rank_col=comm_col.Get_rank()

b_part=np.zeros((row,1))

if rank_row==0:
    comm_col.Bcast([b, N, MPI.DOUBLE], root=0)
comm_row.Scatter([b, row, MPI.DOUBLE], [b_part, row, MPI.DOUBLE], root=0)

x_part=A_part@b_part

if rank_row==0:
    x_sum=np.zeros((row,1))
else:
    x_sum=None
    

comm_row.Reduce([x_part,col,MPI.DOUBLE],[x_sum,col,MPI.DOUBLE],op=MPI.SUM,root=0)

if rank_row==0:
    if rank==0:
        x=np.zeros((N,1))
    else:
        x=None
    comm_col.Gather([x_sum, row, MPI.DOUBLE], [x, row, MPI.DOUBLE], root=0)
if rank==0: 
    print(x)    
    