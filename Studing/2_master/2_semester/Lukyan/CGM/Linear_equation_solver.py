from mpi4py import MPI
import numpy as np
from numpy import int32

comm = MPI.COMM_WORLD
numprocs = comm.Get_size()
rank = comm.Get_rank()

alpha=0.2


#read size of the matix

if rank == 0 :
    f1 = open('size.txt', 'r')
    N = np.array(int32(f1.readline()))
    M = np.array(int32(f1.readline()))
    f1.close()
else:
    M = np.array(0, dtype=int32)
    N = np.array(0, dtype=int32)
  
comm.Bcast([M, 1, MPI.INT], root=0)
comm.Bcast([N, 1, MPI.INT], root=0)

#computing rcount and dispals

if rank == 0 :
    ave, res = divmod(N, numprocs)
    rcounts = np.empty(numprocs, dtype=int32)
    displs = np.empty(numprocs, dtype=int32)
    for k in range(numprocs) : 
        if k < res :
            rcounts[k] = ave + 1
        else :
            rcounts[k] = ave
        if k!=0: 
            displs[k] = displs[k-1] + rcounts[k-1]
        else:
            displs[0]=0
else :
    rcounts = None
    displs = None

N_part=np.array(0)    
comm.Scatter([rcounts, 1, MPI.INT], [N_part, 1, MPI.INT], root=0)



#reading and scattering A and b


if rank ==0 :
    f2=open("A.txt","r")
    for i in range(numprocs):
        A_part=np.zeros((rcounts[i],M))
        for n in range(rcounts[i]):
            for m in range(M):
                A_part[n][m]=float(f2.readline())
        if i==0:
            A_part_0=A_part
        else:
            comm.Send([A_part, rcounts[i]*M, MPI.DOUBLE], dest=i, tag=0)  
    f2.close()
    A_part=A_part_0
    A_part_0=None
    b=np.zeros(N)
    
    f3=open("b.txt","r")
    for i in range(N):
        b[i]=float(f3.readline())
    
else :
    b=None
    A_part = np.zeros((N_part, M))
    comm.Recv([A_part, N_part*M, MPI.DOUBLE], source=0, tag=0)

b_part=np.zeros((N_part,1))

comm.Scatterv([b, rcounts, displs, MPI.DOUBLE],[b_part, N_part, MPI.DOUBLE],root=0)


# initial guess generating and broadcasting

if rank==0:
    x=np.array([[np.random.random()-0.5] for i in range(M)])
else:
    x=np.zeros((M,1))

comm.Bcast([x, M, MPI.DOUBLE], root=0)      


# conjugated gradients method

steps=M
p=np.zeros((M,1))
r=np.zeros((M,1))
q=np.zeros((M,1))


for i in range(steps):
    if i==0:
        r_part=A_part.T@(A_part@x-b_part)
        comm.Allreduce([r_part,M,MPI.DOUBLE],[r,M,MPI.DOUBLE],op=MPI.SUM)
        r+=alpha*x
    else:
        r=r-(q/pq)
    p+=r/(r.T@r)
    q_part=A_part.T@A_part@p
    comm.Allreduce([q_part,M,MPI.DOUBLE],[q,M,MPI.DOUBLE],op=MPI.SUM)
    q+=alpha*p
    pq=(p.T@q)[0][0]
    x=x-p/pq

    
if rank==0:
    with open('x.txt', 'w', encoding='utf-8') as f:
        for i in range(M):
            f.write(str(x[i][0])+"\n")
       
        
        


        

    
