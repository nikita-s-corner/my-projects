import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as opt
from load_data import load_data

file_name = 'Data.xlsx'
sheet = 'Calibration'

data_R_H = load_data(file_name, sheet, 'M', 3, 12) + load_data(file_name, sheet, 'N', 3, 12)
data_R_V = load_data(file_name, sheet, 'K', 13, 22) + load_data(file_name, sheet, 'L', 13, 22)

mean_R_H = data_R_H.mean()
mean_R_V = data_R_V.mean()
error_R_H = data_R_H.std() / np.sqrt(len(data_R_H))
error_R_V = data_R_V.std() / np.sqrt(len(data_R_V))

print(mean_R_H, error_R_H)
print(mean_R_V, error_R_V)

mean_gamma = mean_R_H / mean_R_V
error_gamma = (1 / (mean_R_V ** 2)) * np.sqrt((mean_R_V * error_R_H) ** 2 + (mean_R_H * error_R_V) ** 2)

print(mean_gamma, error_gamma)
