import numpy as np
from funcs_for_tensors import col


def sqrt_matrix(matrix):
    eigens = np.linalg.eig(matrix)
    u = eigens[1]
    d = np.diag(eigens[0])
    return np.dot(u, np.dot(np.sqrt(d), np.linalg.inv(u)))


def fidelity(ro_prep, ro_meas):
    sqrt_prep = sqrt_matrix(ro_prep)
    sqrt_meas = sqrt_matrix(ro_meas)
    return ((np.trace(sqrt_matrix(np.dot(sqrt_prep, np.dot(ro_meas, sqrt_prep))))) ** 2).real


def get_ro(s1, s2, s3):
    return 0.5 * np.array([[1 + s1, s2 - 1j * s3], [s2 + 1j * s3, 1 - s1]])


def get_p(ro):
    return np.trace(np.dot(ro, ro)).real


def eval_s(r_hh, r_hv, r_vh, r_vv):
    gamma = 0.80561
    return ((r_hh + r_hv) - gamma * (r_vh + r_vv)) / ((r_hh + r_hv) + gamma * (r_vh + r_vv))


def eval_si(r_hh, r_hv, r_vh, r_vv):
    gamma = 0.80561
    return (r_hh + r_hv) - gamma * (r_vh + r_vv)


def eval_s0(r_hh, r_hv, r_vh, r_vv):
    gamma = 0.80561
    return (r_hh + r_hv) + gamma * (r_vh + r_vv)


def get_eig_vectors(ro):
    return np.linalg.eig(ro)[1].T[0], np.linalg.eig(ro)[1].T[1]


def get_eig_values(ro):
    return np.linalg.eig(ro)[0][0], np.linalg.eig(ro)[0][1]


def get_hwp(phi):
    return np.array([[np.cos(2 * phi), np.sin(2 * phi)], [np.sin(2 * phi), -np.cos(2 * phi)]])


def get_qwp(phi):
    return np.array([[np.cos(phi) ** 2 + 1j * np.sin(phi) ** 2, (1 - 1j) * np.cos(phi) * np.sin(phi)],
                     [(1 - 1j) * np.cos(phi) * np.sin(phi), 1j * np.cos(phi) ** 2 + np.sin(phi) ** 2]])


def get_polarizer(phi):
    return np.array([[np.cos(phi) ** 2, np.cos(phi) * np.sin(phi)],
                     [np.cos(phi) * np.sin(phi), np.sin(phi) ** 2]])


def get_random_array(array, size_get_array):
    indices = np.random.rand(size_get_array) * (len(array) - 1)
    for i in range(len(indices)):
        indices[i] = int(indices[i])
    int_indices = []
    for i in range(len(indices)):
        int_indices.append(int(indices[i]))
    get_array = []
    for i in range(len(int_indices)):
        get_array.append(array[int_indices[i]])
    return np.array(get_array)


def get_ro_alice_0(phi):
    return np.array([[(np.sin(2 * phi)) ** 2, 0], [0, (np.cos(2 * phi)) ** 2]])


def get_u_alice():
    phi_1 = -14
    phi_2 = 38
    phi_1 = phi_1 * np.pi / 180
    phi_2 = phi_2 * np.pi / 180
    u = np.dot(get_hwp(phi_1), get_qwp(phi_2))
    return u


def get_ro_alice_final(phi):
    u = get_u_alice()
    return np.dot(u, np.dot(get_ro_alice_0(phi), u.T.conj()))


def fidelity_for_matrix(e_1, e_2):
    vec_1 = col(e_1)
    vec_2 = col(e_2)
    mod_vec_1 = np.sqrt(np.sum(np.abs(e_1) ** 2))
    mod_vec_2 = np.sqrt(np.sum(np.abs(e_2) ** 2))
    scalar = np.dot(vec_1.T.conj(), vec_2)
    return (np.abs(scalar / (mod_vec_1 * mod_vec_2)) ** 2)[0][0]
