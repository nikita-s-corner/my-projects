import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as opt
from load_data import load_data
from funcs_for_matrix import *
from funcs_for_tensors import tensor_matrix_product, row, col, col_reverse

file_name = 'Data.xlsx'
sheet = 'QWP_30'

list_rho_input = []
M_exp = []

# H state
print('H state')
ro_prep = np.array([[1, 0], [0, 0]])

r_hh_1 = load_data(file_name, sheet, 'K', 3, 5)
r_hv_1 = load_data(file_name, sheet, 'L', 3, 5)
r_vh_1 = load_data(file_name, sheet, 'M', 3, 5)
r_vv_1 = load_data(file_name, sheet, 'N', 3, 5)

r_hh_2 = load_data(file_name, sheet, 'K', 6, 8)
r_hv_2 = load_data(file_name, sheet, 'L', 6, 8)
r_vh_2 = load_data(file_name, sheet, 'M', 6, 8)
r_vv_2 = load_data(file_name, sheet, 'N', 6, 8)

r_hh_3 = load_data(file_name, sheet, 'K', 9, 11)
r_hv_3 = load_data(file_name, sheet, 'L', 9, 11)
r_vh_3 = load_data(file_name, sheet, 'M', 9, 11)
r_vv_3 = load_data(file_name, sheet, 'N', 9, 11)

s_0_mean_1 = 26945.20619333333
s_0_error_1 = 163.84151258000767
s_1_mean = eval_si(r_hh_1, r_hv_1, r_vh_1, r_vv_1).mean()
s_1_error = eval_si(r_hh_1, r_hv_1, r_vh_1, r_vv_1).std()

s_0_mean_2 = 23226.69506
s_0_error_2 = 152.91642069955546
s_2_mean = eval_si(r_hh_2, r_hv_2, r_vh_2, r_vv_2).mean()
s_2_error = eval_si(r_hh_2, r_hv_2, r_vh_2, r_vv_2).std()

s_0_mean_3 = 24028.255463333335
s_0_error_3 = 319.72990405272185
s_3_mean = eval_si(r_hh_3, r_hv_3, r_vh_3, r_vv_3).mean()
s_3_error = eval_si(r_hh_3, r_hv_3, r_vh_3, r_vv_3).std()

s_0_mean_1_process = eval_s0(r_hh_1, r_hv_1, r_vh_1, r_vv_1).mean()
s_0_error_1_process = eval_s0(r_hh_1, r_hv_1, r_vh_1, r_vv_1).std()
s_0_mean_1_process /= s_0_mean_1
s_0_error_1_process = np.sqrt(((1 / s_0_mean_1) ** 2) * (s_0_error_1 ** 2) +
                              ((s_0_mean_1_process / s_0_mean_1) ** 2) * (s_0_error_1_process ** 2))

s_1_mean /= s_0_mean_1
s_2_mean /= s_0_mean_2
s_3_mean /= s_0_mean_3
s_1_error = np.sqrt(((1 / s_0_mean_1) ** 2) * (s_0_error_1 ** 2) + ((s_1_mean / s_0_mean_1) ** 2) * (s_1_error ** 2))
s_2_error = np.sqrt(((1 / s_0_mean_2) ** 2) * (s_0_error_2 ** 2) + ((s_2_mean / s_0_mean_2) ** 2) * (s_2_error ** 2))
s_3_error = np.sqrt(((1 / s_0_mean_3) ** 2) * (s_0_error_3 ** 2) + ((s_3_mean / s_0_mean_3) ** 2) * (s_3_error ** 2))

print('s_0 = ', s_0_mean_1_process, '+-', s_0_error_1_process)
print('s_1 = ', s_1_mean, '+-', s_1_error)
print('s_2 = ', s_2_mean, '+-', s_2_error)
print('s_3 = ', s_3_mean, '+-', s_3_error)

print('purity = ', (1 + s_1_mean ** 2 + s_2_mean ** 2 + s_3_mean ** 2) / 2)

s_mean = np.sqrt(s_1_mean ** 2 + s_2_mean ** 2 + s_3_mean ** 2)
print('s_mean = ', s_mean)

s_1_mean = s_1_mean / s_mean
s_2_mean = s_2_mean / s_mean
s_3_mean = s_3_mean / s_mean

# print('s_1 = ', s_1_mean)
# print('s_2 = ', s_2_mean)
# print('s_3 = ', s_3_mean)

ro_meas = get_ro(s_1_mean, s_2_mean, s_3_mean)
print('ro_meas = ', ro_meas)

print('Fidelity = ', fidelity(ro_prep, ro_meas))

print()

ro_meas_input = [[0.99085861 + 0.j, 0.09502641 + 0.00527314j], [0.09502641 - 0.00527314j, 0.00914139 + 0.j]]
list_rho_input.append(ro_meas_input)
M_exp.append([s_0_mean_1_process, s_1_mean, s_2_mean, s_3_mean])

# V state
print('V state')
ro_prep = np.array([[0, 0], [0, 1]])

r_hh_1 = load_data(file_name, sheet, 'K', 12, 14)
r_hv_1 = load_data(file_name, sheet, 'L', 12, 14)
r_vh_1 = load_data(file_name, sheet, 'M', 12, 14)
r_vv_1 = load_data(file_name, sheet, 'N', 12, 14)

r_hh_2 = load_data(file_name, sheet, 'K', 15, 17)
r_hv_2 = load_data(file_name, sheet, 'L', 15, 17)
r_vh_2 = load_data(file_name, sheet, 'M', 15, 17)
r_vv_2 = load_data(file_name, sheet, 'N', 15, 17)

r_hh_3 = load_data(file_name, sheet, 'K', 18, 20)
r_hv_3 = load_data(file_name, sheet, 'L', 18, 20)
r_vh_3 = load_data(file_name, sheet, 'M', 18, 20)
r_vv_3 = load_data(file_name, sheet, 'N', 18, 20)

s_0_mean_1 = 15197.160713333336
s_0_error_1 = 141.93319834042924
s_1_mean = eval_si(r_hh_1, r_hv_1, r_vh_1, r_vv_1).mean()
s_1_error = eval_si(r_hh_1, r_hv_1, r_vh_1, r_vv_1).std()

s_0_mean_2 = 15614.822156666667
s_0_error_2 = 193.30494484116147
s_2_mean = eval_si(r_hh_2, r_hv_2, r_vh_2, r_vv_2).mean()
s_2_error = eval_si(r_hh_2, r_hv_2, r_vh_2, r_vv_2).std()

s_0_mean_3 = 15634.662306666665
s_0_error_3 = 276.3218163009687
s_3_mean = eval_si(r_hh_3, r_hv_3, r_vh_3, r_vv_3).mean()
s_3_error = eval_si(r_hh_3, r_hv_3, r_vh_3, r_vv_3).std()

s_0_mean_1_process = eval_s0(r_hh_1, r_hv_1, r_vh_1, r_vv_1).mean()
s_0_error_1_process = eval_s0(r_hh_1, r_hv_1, r_vh_1, r_vv_1).std()
s_0_mean_1_process /= s_0_mean_1
s_0_error_1_process = np.sqrt(((1 / s_0_mean_1) ** 2) * (s_0_error_1 ** 2) +
                              ((s_0_mean_1_process / s_0_mean_1) ** 2) * (s_0_error_1_process ** 2))

s_1_mean /= s_0_mean_1
s_2_mean /= s_0_mean_2
s_3_mean /= s_0_mean_3
s_1_error = np.sqrt(((1 / s_0_mean_1) ** 2) * (s_0_error_1 ** 2) + ((s_1_mean / s_0_mean_1) ** 2) * (s_1_error ** 2))
s_2_error = np.sqrt(((1 / s_0_mean_2) ** 2) * (s_0_error_2 ** 2) + ((s_2_mean / s_0_mean_2) ** 2) * (s_2_error ** 2))
s_3_error = np.sqrt(((1 / s_0_mean_3) ** 2) * (s_0_error_3 ** 2) + ((s_3_mean / s_0_mean_3) ** 2) * (s_3_error ** 2))

print('s_0 = ', s_0_mean_1_process, '+-', s_0_error_1_process)
print('s_1 = ', s_1_mean, '+-', s_1_error)
print('s_2 = ', s_2_mean, '+-', s_2_error)
print('s_3 = ', s_3_mean, '+-', s_3_error)

print('purity = ', (1 + s_1_mean ** 2 + s_2_mean ** 2 + s_3_mean ** 2) / 2)

s_mean = np.sqrt(s_1_mean ** 2 + s_2_mean ** 2 + s_3_mean ** 2)
print('s_mean = ', s_mean)

s_1_mean = s_1_mean / s_mean
s_2_mean = s_2_mean / s_mean
s_3_mean = s_3_mean / s_mean

# print('s_1 = ', s_1_mean)
# print('s_2 = ', s_2_mean)
# print('s_3 = ', s_3_mean)

ro_meas = get_ro(s_1_mean, s_2_mean, s_3_mean)
print('ro_meas = ', ro_meas)

print('Fidelity = ', fidelity(ro_prep, ro_meas))

print()

ro_meas_input = [[0.00254886 + 0.j, -0.04485471 + 0.02303082j], [-0.04485471 - 0.02303082j, 0.99745114 + 0.j]]
list_rho_input.append(ro_meas_input)
M_exp.append([s_0_mean_1_process, s_1_mean, s_2_mean, s_3_mean])

# D state
print('D state')
ro_prep = 0.5 * np.array([[1, 1], [1, 1]])

r_hh_1 = load_data(file_name, sheet, 'K', 21, 23)
r_hv_1 = load_data(file_name, sheet, 'L', 21, 23)
r_vh_1 = load_data(file_name, sheet, 'M', 21, 23)
r_vv_1 = load_data(file_name, sheet, 'N', 21, 23)

r_hh_2 = load_data(file_name, sheet, 'K', 24, 26)
r_hv_2 = load_data(file_name, sheet, 'L', 24, 26)
r_vh_2 = load_data(file_name, sheet, 'M', 24, 26)
r_vv_2 = load_data(file_name, sheet, 'N', 24, 26)

r_hh_3 = load_data(file_name, sheet, 'K', 27, 29)
r_hv_3 = load_data(file_name, sheet, 'L', 27, 29)
r_vh_3 = load_data(file_name, sheet, 'M', 27, 29)
r_vv_3 = load_data(file_name, sheet, 'N', 27, 29)

s_0_mean_1 = 13727.250486666666
s_0_error_1 = 218.8490219738275
s_1_mean = eval_si(r_hh_1, r_hv_1, r_vh_1, r_vv_1).mean()
s_1_error = eval_si(r_hh_1, r_hv_1, r_vh_1, r_vv_1).std()

s_0_mean_2 = 15473.929366666665
s_0_error_2 = 105.37645812856958
s_2_mean = eval_si(r_hh_2, r_hv_2, r_vh_2, r_vv_2).mean()
s_2_error = eval_si(r_hh_2, r_hv_2, r_vh_2, r_vv_2).std()

s_0_mean_3 = 14162.744206666668
s_0_error_3 = 112.42315718325662
s_3_mean = eval_si(r_hh_3, r_hv_3, r_vh_3, r_vv_3).mean()
s_3_error = eval_si(r_hh_3, r_hv_3, r_vh_3, r_vv_3).std()

s_0_mean_1_process = eval_s0(r_hh_1, r_hv_1, r_vh_1, r_vv_1).mean()
s_0_error_1_process = eval_s0(r_hh_1, r_hv_1, r_vh_1, r_vv_1).std()
s_0_mean_1_process /= s_0_mean_1
s_0_error_1_process = np.sqrt(((1 / s_0_mean_1) ** 2) * (s_0_error_1 ** 2) +
                              ((s_0_mean_1_process / s_0_mean_1) ** 2) * (s_0_error_1_process ** 2))

s_1_mean /= s_0_mean_1
s_2_mean /= s_0_mean_2
s_3_mean /= s_0_mean_3
s_1_error = np.sqrt(((1 / s_0_mean_1) ** 2) * (s_0_error_1 ** 2) + ((s_1_mean / s_0_mean_1) ** 2) * (s_1_error ** 2))
s_2_error = np.sqrt(((1 / s_0_mean_2) ** 2) * (s_0_error_2 ** 2) + ((s_2_mean / s_0_mean_2) ** 2) * (s_2_error ** 2))
s_3_error = np.sqrt(((1 / s_0_mean_3) ** 2) * (s_0_error_3 ** 2) + ((s_3_mean / s_0_mean_3) ** 2) * (s_3_error ** 2))

print('s_0 = ', s_0_mean_1_process, '+-', s_0_error_1_process)
print('s_1 = ', s_1_mean, '+-', s_1_error)
print('s_2 = ', s_2_mean, '+-', s_2_error)
print('s_3 = ', s_3_mean, '+-', s_3_error)

print('purity = ', (1 + s_1_mean ** 2 + s_2_mean ** 2 + s_3_mean ** 2) / 2)

s_mean = np.sqrt(s_1_mean ** 2 + s_2_mean ** 2 + s_3_mean ** 2)
print('s_mean = ', s_mean)

s_1_mean = s_1_mean / s_mean
s_2_mean = s_2_mean / s_mean
s_3_mean = s_3_mean / s_mean

# print('s_1 = ', s_1_mean)
# print('s_2 = ', s_2_mean)
# print('s_3 = ', s_3_mean)

ro_meas = get_ro(s_1_mean, s_2_mean, s_3_mean)
print('ro_meas = ', ro_meas)

print('Fidelity = ', fidelity(ro_prep, ro_meas))

print()

ro_meas_input = [[0.55218384 + 0.j, 0.4965379 + 0.02696217j], [0.4965379 - 0.02696217j, 0.44781616 + 0.j]]
list_rho_input.append(ro_meas_input)
M_exp.append([s_0_mean_1_process, s_1_mean, s_2_mean, s_3_mean])

# R state
print('R state')
ro_prep = 0.5 * np.array([[1, -1j], [1j, 1]])

r_hh_1 = load_data(file_name, sheet, 'K', 30, 32)
r_hv_1 = load_data(file_name, sheet, 'L', 30, 32)
r_vh_1 = load_data(file_name, sheet, 'M', 30, 32)
r_vv_1 = load_data(file_name, sheet, 'N', 30, 32)

r_hh_2 = load_data(file_name, sheet, 'K', 33, 35)
r_hv_2 = load_data(file_name, sheet, 'L', 33, 35)
r_vh_2 = load_data(file_name, sheet, 'M', 33, 35)
r_vv_2 = load_data(file_name, sheet, 'N', 33, 35)

r_hh_3 = load_data(file_name, sheet, 'K', 36, 38)
r_hv_3 = load_data(file_name, sheet, 'L', 36, 38)
r_vh_3 = load_data(file_name, sheet, 'M', 36, 38)
r_vv_3 = load_data(file_name, sheet, 'N', 36, 38)

s_0_mean_1 = 16690.342723333335
s_0_error_1 = 350.731155659827
s_1_mean = eval_si(r_hh_1, r_hv_1, r_vh_1, r_vv_1).mean()
s_1_error = eval_si(r_hh_1, r_hv_1, r_vh_1, r_vv_1).std()

s_0_mean_2 = 15506.3413
s_0_error_2 = 337.8821518974913
s_2_mean = eval_si(r_hh_2, r_hv_2, r_vh_2, r_vv_2).mean()
s_2_error = eval_si(r_hh_2, r_hv_2, r_vh_2, r_vv_2).std()

s_0_mean_3 = 17375.15638
s_0_error_3 = 677.5306120628026
s_3_mean = eval_si(r_hh_3, r_hv_3, r_vh_3, r_vv_3).mean()
s_3_error = eval_si(r_hh_3, r_hv_3, r_vh_3, r_vv_3).std()

s_0_mean_1_process = eval_s0(r_hh_1, r_hv_1, r_vh_1, r_vv_1).mean()
s_0_error_1_process = eval_s0(r_hh_1, r_hv_1, r_vh_1, r_vv_1).std()
s_0_mean_1_process /= s_0_mean_1
s_0_error_1_process = np.sqrt(((1 / s_0_mean_1) ** 2) * (s_0_error_1 ** 2) +
                              ((s_0_mean_1_process / s_0_mean_1) ** 2) * (s_0_error_1_process ** 2))

s_1_mean /= s_0_mean_1
s_2_mean /= s_0_mean_2
s_3_mean /= s_0_mean_3
s_1_error = np.sqrt(((1 / s_0_mean_1) ** 2) * (s_0_error_1 ** 2) + ((s_1_mean / s_0_mean_1) ** 2) * (s_1_error ** 2))
s_2_error = np.sqrt(((1 / s_0_mean_2) ** 2) * (s_0_error_2 ** 2) + ((s_2_mean / s_0_mean_2) ** 2) * (s_2_error ** 2))
s_3_error = np.sqrt(((1 / s_0_mean_3) ** 2) * (s_0_error_3 ** 2) + ((s_3_mean / s_0_mean_3) ** 2) * (s_3_error ** 2))

print('s_0 = ', s_0_mean_1_process, '+-', s_0_error_1_process)
print('s_1 = ', s_1_mean, '+-', s_1_error)
print('s_2 = ', s_2_mean, '+-', s_2_error)
print('s_3 = ', s_3_mean, '+-', s_3_error)

print('purity = ', (1 + s_1_mean ** 2 + s_2_mean ** 2 + s_3_mean ** 2) / 2)

s_mean = np.sqrt(s_1_mean ** 2 + s_2_mean ** 2 + s_3_mean ** 2)
print('s_mean = ', s_mean)

s_1_mean = s_1_mean / s_mean
s_2_mean = s_2_mean / s_mean
s_3_mean = s_3_mean / s_mean

# print('s_1 = ', s_1_mean)
# print('s_2 = ', s_2_mean)
# print('s_3 = ', s_3_mean)

ro_meas = get_ro(s_1_mean, s_2_mean, s_3_mean)
print('ro_meas = ', ro_meas)

print('Fidelity = ', fidelity(ro_prep, ro_meas))

print()

ro_meas_input = [[0.60822786 + 0.j, 0.01561479 - 0.48789641j], [0.01561479 + 0.48789641j, 0.39177214 + 0.j]]
list_rho_input.append(ro_meas_input)
M_exp.append([s_0_mean_1_process, s_1_mean, s_2_mean, s_3_mean])

s0 = np.array([[1, 0], [0, 1]])
s1 = np.array([[1, 0], [0, -1]])
s2 = np.array([[0, 1], [1, 0]])
s3 = np.array([[0, -1j], [1j, 0]])

list_s = [s0, s1, s2, s3]

# print(list_rho)
# print(list_s)

B = []
for s in list_s:
    for rho in list_rho_input:
        B.append(list(row(tensor_matrix_product(np.conjugate(rho), s))))
B = np.array(B)
# print(B)
M_exp = np.array(M_exp)
print('M_exp = ', M_exp)

chi_exp = col_reverse(np.dot(np.linalg.inv(B), col(M_exp)), 4)
print('chi_exp = ', chi_exp)

phi = -30 * np.pi / 180
U = get_qwp(phi)
print('U = ', U)

M_theory = []
for rho_input in list_rho_input:
    rows = []
    for s in list_s:
        rows.append(np.trace(np.dot(np.dot(U, np.dot(rho_input, U.T.conj())), s)))
    M_theory.append(rows)
M_theory = np.array(M_theory)
print('M_theory = ', M_theory)

chi_theory = col_reverse(np.dot(np.linalg.inv(B), col(M_theory)), 4)
print('chi_theory = ', chi_theory)

print('check chi_theory = ', np.dot(col(U), (col(U)).T.conj()))

x = np.linspace(0, 16, 16)
print(x)

fig, ax = plt.subplots()

ax.bar(x - 0.125, np.real(col(M_exp).reshape(-1)), width=0.3, color='red', alpha=0.5, label=r'$M_{exp}$')
ax.bar(x + 0.125, np.real(col(M_theory).reshape(-1)), width=0.3, color='blue', alpha=0.5, label=r'$M_{th}$')

plt.xlabel('n', fontsize=13)
plt.ylabel(r'$M_{ij}$', fontsize=13)

fig.set_figwidth(5)
fig.set_figheight(5)
plt.xlim(-0.5, 16.5)
plt.legend(loc='upper right')
plt.show()
fig.savefig('Picture_qwp_30.pdf')


eig_values_chi, eig_vectors_chi = np.linalg.eig(chi_exp)
print(eig_values_chi, eig_vectors_chi)
print('U = ', U)
eig_value_chi = eig_values_chi[0]
eig_vector_chi = eig_vectors_chi.T[0]
U_exp = np.sqrt(eig_value_chi) * col_reverse(eig_vector_chi, 2)
print('norma = ', np.sqrt(np.sum(np.abs(U_exp) ** 2)))
print('E_exp = ', U_exp)
print('fidelity = ', fidelity_for_matrix(U, U_exp))

# E_theory
print('U = ', U)
print('eigen = ', np.linalg.eig(U))

# E_exp
print('U_exp = ', U_exp)
print('eigen_exp = ', np.linalg.eig(U_exp))
