import numpy as np


def func_sin(x, b, a, k, phi):
    return b + a * np.sin(k * x + phi)


def func_1_inter(x, c, a, x0, w):
    return c * (1 + a * (np.sinc((x - x0) / (np.pi * w))) ** 2)


def func_2_inter(x, c, a, x0, w):
    return c * (1 - a * np.sinc((x - x0) / (np.pi * w)))


def func_2_inter_pik(x, c, a, x0, w):
    return c * (1 + a * np.sinc((x - x0) / (np.pi * w)))
