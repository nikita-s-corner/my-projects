import openpyxl
import numpy as np


def load_data(file_name, sheet_name, column, min_row, max_row):
    wb = openpyxl.load_workbook(filename=file_name)
    sheet = wb[sheet_name]
    data = []
    for i in range(min_row, max_row + 1, 1):
        data.append(float(sheet[column + str(i)].value))
    return np.array(data)
