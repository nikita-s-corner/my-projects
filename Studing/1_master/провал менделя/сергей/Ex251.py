import matplotlib.pyplot as plt
import numpy as np
from load_data import load_data

file_name = 'Data.xlsx'
sheet = '251'

data_x = np.linspace(0, 10, 100)
data_SC1 = load_data(file_name, sheet, 'B', 5, 104)
data_SC2 = load_data(file_name, sheet, 'C', 5, 104) + load_data(file_name, sheet, 'D', 5, 104)

fig, ax = plt.subplots()
plt.plot(data_x, data_SC1, label=r'$T_1$')
plt.plot(data_x, data_SC2, label=r'$T_2 + R_2$')
plt.tick_params(which='major', direction='in')
plt.tick_params(which='minor', direction='in')

plt.legend(loc='upper right')

plt.xlim(0, 10)
plt.ylim(0, 600)
ax.minorticks_off()
plt.xlabel(r'$x$'+', мкм', fontsize=13)
plt.ylabel('Единичные отсчёты', fontsize=13)
plt.show()
fig.savefig('Ex_251.pdf')
