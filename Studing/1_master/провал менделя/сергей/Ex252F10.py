import matplotlib.pyplot as plt
import numpy as np
import scipy.optimize as opt
from load_data import load_data
from func import func_sin, func_1_inter

file_name = 'Data.xlsx'
sheet = '252F10'

data_x = 1000 * load_data(file_name, sheet, 'B', 3, 803)
data_SC1 = load_data(file_name, sheet, 'C', 3, 803)
data_SC2 = load_data(file_name, sheet, 'D', 3, 803) + load_data(file_name, sheet, 'E', 3, 803)

fig, ax = plt.subplots()
plt.plot(data_x, data_SC1, label=r'$T_1$')
plt.plot(data_x, data_SC2, label=r'$T_2 + R_2$')
plt.tick_params(which='major', direction='in')
plt.tick_params(which='minor', direction='in')

plt.legend(loc='upper right')

plt.xlim(-200, 200)
plt.ylim(0, 600)
ax.minorticks_off()
plt.xlabel(r'$x$'+', мкм', fontsize=13)
plt.ylabel('Единичные отсчёты', fontsize=13)
plt.show()
fig.savefig('Ex_252F10_1.pdf')

x_lin = np.linspace(-10, 10, 1000)
p0 = [347.0, 100.0, 5.0, 0.92]
data_x_center = data_x[380:420]
data_SC1_center = data_SC1[380:420]
popt, pcov = opt.curve_fit(func_sin, data_x_center, data_SC1_center, p0=p0)
print('popt for SC1', popt)
print('pcov for SC1', pcov)
print('error for SC1', np.sqrt(np.diag(pcov)))

average_y = data_SC1_center.mean()
r = np.sum((func_sin(data_x_center, *popt) - average_y) ** 2) / np.sum((data_SC1_center - average_y) ** 2)
print('R for SC1', r)

y_model = func_sin(x_lin, *popt)
fig, ax = plt.subplots()
plt.plot(data_x_center, data_SC1_center, 'bo', label='Эксперимент')
plt.plot(x_lin, y_model, 'r--', label='Аппроксимация')
plt.legend(loc='upper right')

plt.xlim(-5, 5)
plt.ylim(0, 600)
ax.minorticks_off()
plt.xlabel(r'$x$'+', мкм', fontsize=13)
plt.ylabel('Единичные отсчёты', fontsize=13)
plt.tick_params(which='major', direction='in')
plt.tick_params(which='minor', direction='in')
plt.show()
fig.savefig('Ex_252F10_2_T1.pdf')

k = popt[2]
lam = 2 * np.pi / k
error_lam = 2 * np.pi * np.sqrt(np.diag(pcov))[2] / (k ** 2)
print('lambda_1 =', lam, ' ', 'error lambda_1 =', error_lam)

print()

x_lin = np.linspace(-10, 10, 1000)
p0 = [250.0, 75.0, 5.0, 1.0]
data_x_center = data_x[380:420]
data_SC2_center = data_SC2[380:420]
popt, pcov = opt.curve_fit(func_sin, data_x_center, data_SC2_center, p0=p0)
print('popt for SC2', popt)
print('pcov for SC2', pcov)
print('error for SC2', np.sqrt(np.diag(pcov)))

average_y = data_SC2_center.mean()
r = np.sum((func_sin(data_x_center, *popt) - average_y) ** 2) / np.sum((data_SC2_center - average_y) ** 2)
print('R for SC2', r)

y_model = func_sin(x_lin, *popt)
fig, ax = plt.subplots()
plt.plot(data_x_center, data_SC2_center, 'bo', label='Эксперимент')
plt.plot(x_lin, y_model, 'g--', label='Аппроксимация')
plt.legend(loc='upper right')

plt.xlim(-5, 5)
plt.ylim(0, 600)
ax.minorticks_off()
plt.xlabel(r'$x$'+', мкм', fontsize=13)
plt.ylabel('Единичные отсчёты', fontsize=13)
plt.tick_params(which='major', direction='in')
plt.tick_params(which='minor', direction='in')
plt.show()
fig.savefig('Ex_252F10_2_T2R2.pdf')

k = popt[2]
lam = 2 * np.pi / k
print(lam)
error_lam = 2 * np.pi * np.sqrt(np.diag(pcov))[2] / (k ** 2)
print('lambda_2 =', lam, ' ', 'error lambda_2 =', error_lam)

print()

data_x_disp = data_x[50:750]
data_SC1_disp = data_SC1[50:750]
data_SC2_disp = data_SC2[50:750]

for i in range(0, 700, 1):
    data_SC1_disp[i] = (data_SC1[(50 + i - 3):(50 + i + 3)].std())
    data_SC2_disp[i] = (data_SC2[(50 + i - 3):(50 + i + 3)].std())
data_SC1_disp = data_SC1_disp ** 2
data_SC2_disp = data_SC2_disp ** 2

fig, ax = plt.subplots()
plt.plot(data_x_disp, data_SC1_disp, label='Скользящая дисперсия (ширина окна 6) ' + r'$T_1$', color='blue', alpha=0.3)
plt.plot(data_x_disp, data_SC2_disp, label='Скользящая дисперсия (ширина окна 6) ' + r'$T_2 + R_2$', color='red', alpha=0.3)
plt.legend(loc='lower right')


x_lin = np.linspace(-125, 100, 10000)
p0_1 = [15000.0, 0.1, -25.0, 50.0]
p0_2 = [7500.0, 0.1, -25.0, 50.0]
data_x_disp = data_x_disp[150:550]
data_SC1_disp = data_SC1_disp[150:550]
data_SC2_disp = data_SC2_disp[150:550]
popt1, pcov1 = opt.curve_fit(func_1_inter, data_x_disp, data_SC1_disp, p0=p0_1)
popt2, pcov2 = opt.curve_fit(func_1_inter, data_x_disp, data_SC2_disp, p0=p0_2)

print('popt1 = ', popt1)
print('pcov1 = ', pcov1)
print('error1 = ', np.sqrt(np.diag(pcov1)))

print()

print('popt2 = ', popt2)
print('pcov2 = ', pcov2)
print('error2 = ', np.sqrt(np.diag(pcov2)))

average_y = data_SC1_disp.mean()
r = np.sum((func_1_inter(data_x_disp, *popt1) - average_y) ** 2) / np.sum((data_SC1_disp - average_y) ** 2)
print('R = ', r)

print()

average_y = data_SC2_disp.mean()
r = np.sum((func_1_inter(data_x_disp, *popt2) - average_y) ** 2) / np.sum((data_SC2_disp - average_y) ** 2)
print('R = ', r)

y_model_1 = func_1_inter(x_lin, *popt1)
y_model_2 = func_1_inter(x_lin, *popt2)

plt.plot(x_lin, y_model_1, 'b--', label='Аппроксимация ' + r'$T_1$')
plt.plot(x_lin, y_model_2, 'r--', label='Аппроксимация ' + r'$T_2 + R_2$')

plt.xlim(-125, 100)
plt.ylim(0, 25000)
ax.minorticks_off()
plt.xlabel(r'$x$'+', мкм', fontsize=13)
plt.ylabel('Дисперсия единичных отсчётов', fontsize=13)
plt.tick_params(which='major', direction='in')
plt.tick_params(which='minor', direction='in')
plt.show()

fig.savefig('Ex_252F10_3.pdf')
