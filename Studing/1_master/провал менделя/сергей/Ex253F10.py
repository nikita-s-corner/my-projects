import matplotlib.pyplot as plt
import numpy as np
import scipy.optimize as opt
from load_data import load_data
from func import func_sin, func_1_inter, func_2_inter, func_2_inter_pik

file_name = 'Data.xlsx'
sheet = '253F10'

# Интерференционная картина в единичных отсчётах

data_x = 1000 * load_data(file_name, sheet, 'B', 3, 403)
data_SC1 = load_data(file_name, sheet, 'C', 3, 403)
data_SC2 = load_data(file_name, sheet, 'D', 3, 403) + load_data(file_name, sheet, 'E', 3, 403)

fig, ax = plt.subplots()
plt.plot(data_x, data_SC1, label=r'$T_1$', alpha=0.5)
plt.plot(data_x, data_SC2, label=r'$T_2 + R_2$', alpha=0.5)
plt.tick_params(which='major', direction='in')
plt.tick_params(which='minor', direction='in')

plt.legend(loc='upper right')

plt.xlim(-100, 100)
ax.minorticks_off()
plt.xlabel(r'$x$'+', мкм', fontsize=13)
plt.ylabel('Единичные отсчёты', fontsize=13)
plt.show()
fig.savefig('Ex_253F10_4.pdf')

# Двухфотонная интерференция T_1 - T_2 + T_1 - R_2

data_x = 1000 * load_data(file_name, sheet, 'B', 3, 403)
data_T1T2T1R2 = load_data(file_name, sheet, 'L', 3, 403)

x_lin = np.linspace(-100, 100, 10000)
p0 = [7.0, 5.0, -10.0, 10.0]
data_x_mean = data_x[10:390]
data_T1T2T1R2_mean = data_T1T2T1R2[10:390]
for i in range(0, 379, 1):
    data_T1T2T1R2_mean[i] = data_T1T2T1R2[10 + i - 3:10 + i + 3].mean()
popt, pcov = opt.curve_fit(func_2_inter, data_x_mean, data_T1T2T1R2_mean, p0=p0)
print('popt for SC1', popt)
print('pcov for SC1', pcov)
print('error for SC1', np.sqrt(np.diag(pcov)))

average_y = data_T1T2T1R2_mean.mean()
r = np.sum((func_2_inter(data_x_mean, *popt) - average_y) ** 2) / np.sum((data_T1T2T1R2_mean - average_y) ** 2)
print('R for SC1', r)

y_model = func_2_inter(x_lin, *popt)
fig, ax = plt.subplots()
plt.plot(data_x_mean, data_T1T2T1R2_mean, label='Эксперимент ' + r'$T_1:T_2 + T_1:R_2$', color='blue', alpha=0.3)
plt.plot(x_lin, y_model, 'r--', label='Аппроксимация ' + r'$T_1:T_2 + T_1:R_2$')
plt.legend(loc='upper right')

ax.minorticks_off()
plt.xlabel(r'$x$'+', мкм', fontsize=13)
plt.ylabel('Число совпадений', fontsize=13)
plt.tick_params(which='major', direction='in')
plt.tick_params(which='minor', direction='in')
plt.show()
fig.savefig('Ex_253F10_5.pdf')

# Двухфотонная интерференция T_2 - R_2

data_x = 1000 * load_data(file_name, sheet, 'B', 3, 403)
data_T2R2 = load_data(file_name, sheet, 'J', 3, 403)

x_lin = np.linspace(-100, 100, 10000)
p0 = [1.0, 3.0, -10.0, 50.0]
data_x_mean = data_x[10:390]
data_T2R2_mean = data_T2R2[10:390]
for i in range(0, 379, 1):
    data_T2R2_mean[i] = data_T2R2[10 + i - 3:10 + i + 3].mean()
popt, pcov = opt.curve_fit(func_2_inter_pik, data_x_mean, data_T2R2_mean, p0=p0)
print('popt for SC1', popt)
print('pcov for SC1', pcov)
print('error for SC1', np.sqrt(np.diag(pcov)))

average_y = data_T2R2_mean.mean()
r = np.sum((func_2_inter_pik(data_x_mean, *popt) - average_y) ** 2) / np.sum((data_T2R2_mean - average_y) ** 2)
print('R for SC1', r)

y_model = func_2_inter_pik(x_lin, *popt)
fig, ax = plt.subplots()
plt.plot(data_x_mean, data_T2R2_mean, label='Эксперимент ' + r'$T_2:R_2$', color='blue', alpha=0.3)
plt.plot(x_lin, y_model, 'r--', label='Аппроксимация ' + r'$T_2:R_2$')
plt.legend(loc='upper right')

ax.minorticks_off()
plt.xlabel(r'$x$'+', мкм', fontsize=13)
plt.ylabel('Число совпадений', fontsize=13)
plt.tick_params(which='major', direction='in')
plt.tick_params(which='minor', direction='in')
plt.show()
fig.savefig('Ex_253F10_8.pdf')
