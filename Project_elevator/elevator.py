speed = 3
load_time = 15

class Elevator(object):
    def __init__(self, init_pos, max_occ):
        self.pos = init_pos
        self.status = 2
        self.progress = 0
        self.stops = []
        
        self.cur_occ = 0
        self.cur_pas = set([])
        self.max_occ = max_occ
        self.loading = False
        
    def reschedule(self, stops):
        old_status = self.status
        old_progress = self.progress
        
        if old_status==2:
            self.stops = stops
            if self.pos==self.stops[0]:
                self.stops.pop(0)
                self.status = 0
                self.progress = 0
            else:
                self.status = (-1)**(self.pos>self.stops[0])
                self.progress = 0
        
        elif old_status==0:
            self.stops = stops
            if self.pos==self.stops[0]:
                self.stops.pop(0)
            
        elif old_status==1:
            abs_pos = self.abs_pos()
            if abs_pos==speed*stops[0]:
                stops.pop(0)
                self.status = 0
                self.progress = 0
                
                self.stops = stops
            elif abs_pos<speed*stops[0]:
                self.stops = stops
            else:
                self.status = -1
                self.progress = (-self.progress)%3
                self.pos = self.pos+1 if self.progress>0 else self.pos
                
                self.stops = stops
                
        elif old_status == -1:
            abs_pos = self.abs_pos()
            if abs_pos==speed*stops[0]:
                stops.pop(0)
                self.status = 0
                self.progress = 0
                
                self.stops = stops
            elif abs_pos>speed*stops[0]:
                self.stops = stops
            else:
                self.status = 1
                self.progress = (-self.progress)%3
                self.pos = self.pos-1 if self.progress>0 else self.pos
                
                self.stops = stops
        
    def act(self):
        if self.status == 1:
            self.move_up()
        elif self.status == -1:
            self.move_down()
        elif self.status == 0:
            self.stop()
        elif len(self.stops)>0:
#             self.cur_dir = (-1)**(self.pos>self.stops[0])
#             self.status = self.cur_dir 
            self.status = (-1)**(self.pos>self.stops[0])
            
    def move_up(self):
        self.progress += 1
        if self.progress == speed:
            self.progress = 0
            self.pos += 1
            
            if self.pos==self.stops[0]:
                self.status = 0
#                 self.cur_dir = (-1)**(self.pos>self.stops[1]) if len(self.stops)>1 else 0
                self.stops.pop(0)
    
    def move_down(self):
        self.progress += 1
        if self.progress == speed:
            self.progress = 0
            self.pos -= 1
            
            if self.pos==self.stops[0]:
                self.status = 0
#                 self.cur_dir = (-1)**(self.pos>self.stops[1]) if len(self.stops)>1 else 0
                self.stops.pop(0)
            
    def stop(self):
        if self.progress==0:
            self.loading = True
            
        self.progress += 1
        if self.progress == load_time:
            self.progress = 0
            self.loading = False
            
            if len(self.stops)>0:
                self.status = (-1)**(self.pos>self.stops[0])
            else:
                self.status = 2
#                 self.cur_dir == 0
    
    def load(self, upload, download):
        self.cur_pas = self.cur_pas - upload
        self.cur_pas = self.cur_pas.union(download)
        self.cur_occ = len(self.cur_pas)
        self.loading = False
        
        if self.cur_occ>self.max_occ:
            print("Overload!!!")
        
    def abs_pos(self):
        return self.status*self.progress+speed*self.pos
    
max_occ = 6        
class Building(object):
    def __init__(self, n_floors, n_elevs, pass_arr):
        self.n_floors = n_floors
        self.n_elevs = n_elevs
        self.time = 0
        
        self.elevators = [Elevator(n_floors-1, max_occ) for i in range(n_elevs)]
        self.floor_pas = [set([]) for i in range(n_floors)]
        
        self.pass_arr = pass_arr
        self.result = []
        self.total_pas = len(pass_arr)
        self.last_id = 0
        
    def check_new_pas(self):
        prev = self.last_id
        while self.last_id<self.total_pas and (self.time>=self.pass_arr[self.last_id][0]):
            self.floor_pas[self.pass_arr[self.last_id][1]].add(self.last_id)
            self.last_id += 1
            
        return prev!=self.last_id
            
    def load(self):
        for elev in self.elevators:
            if elev.loading:
                floor = elev.pos
                upload = set([pas for pas in elev.cur_pas if self.pass_arr[pas][2]==floor])
                download = set([pas for pas in self.floor_pas[floor] if self.pass_arr[pas][2] in elev.stops])
                
                max_download = max_occ-(elev.cur_occ-len(upload))
                if len(download)>max_download:
                    download = set(random.sample(a, max_download))
                elev.load(upload, download)
                
                self.floor_pas[floor] = self.floor_pas[floor]-download
                
                for pas in download:
                    self.pass_arr[pas].append(self.time)
                
                for pas in upload:
                    self.pass_arr[pas].append(self.time)
    
    def reschedule(self, stops):
        for i in range(self.n_elevs):
            if stops[i] is not None:
                self.elevators[i].reschedule(stops[i])
                
    def elev_act(self):
        for elev in self.elevators:
            elev.act()
                
    def get_meta(self):
        elev_pas = [elev.cur_pas for elev in self.elevators]
        elev_stat = [elev.status for elev in self.elevators]
        elev_abs_pos = [elev.abs_pos() for elev in self.elevators]
        
        return self.floor_pas, elev_pas, elev_abs_pos, elev_stat
    
    def act(self, n_secs):
        print(self.pass_arr)
        for i in range(n_secs):
            is_changed = self.check_new_pas()

            self.load()

            meta = self.get_meta()
            
            if is_changed:
                new_stops = self.brains(meta)

                self.reschedule(new_stops)
                
            print([elev.abs_pos() for elev in self.elevators])

            self.elev_act()
            
            self.time += 1
    
    def brains(self, meta):
        if self.time==0:
            return [[20, 12, 1], [5, 15, 20], None, None]
        else:
            return [None, [15, 18, 20], None, None]